/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   magivector.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 20:15:49 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/27 20:15:50 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

float			magivector(t_ivector v1)
{
	return (sqrt(dotproductivtoiv(v1, v1)));
}
